﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T employee)
        {
            Data.Add(employee);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(Guid id, T employee)
        {
            for (int i = 0; i < Data.Count; i++)
            {
                if (Data[i].Id == id)
                {
                    Data[i] = employee;
                    break;
                }
            }

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            Data.RemoveAll(x => x.Id == id);
            return Task.CompletedTask;
        }
    }
}